<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveCptCntCards',
            'Hivecptcntcardscardrendercard',
            [
                'Card' => 'renderCard'
            ],
            // non-cacheable actions
            [
                'Card' => 'renderCard'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hivecptcntcardscardrendercard {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_cpt_cnt_cards') . 'Resources/Public/Icons/user_plugin_hivecptcntcardscardrendercard.svg
                        title = LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cards_domain_model_hivecptcntcardscardrendercard
                        description = LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hive_cpt_cnt_cards_domain_model_hivecptcntcardscardrendercard.description
                        tt_content_defValues {
                            CType = list
                            list_type = hivecptcntcards_hivecptcntcardscardrendercard
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
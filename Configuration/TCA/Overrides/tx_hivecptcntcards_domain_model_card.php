<?php

defined('TYPO3_MODE') or die();

$sModel = 'tx_hivecptcntcards_domain_model_card';

//#$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_cnt_bs_btn/Resources/Public/Icons/bootstrap_16x16.gif';
//$GLOBALS['TCA'][$sModel]['columns']['link']['config'] = [
//    'type' => 'input',
//    'size' => 20,
//    'eval' => 'trim',
//    'wizards' => [
//        '_PADDING' => 2,
//        'link' => [
//            'type' => 'popup',
//            'title' => 'Link',
//            'icon' => 'link_popup.gif',
//            'module' => [
//                'name' => 'wizard_element_browser',
//                'urlParameters' => [
//                    'mode' => 'wizard'
//                ]
//            ],
//            'JSopenParams' => 'height=800,width=600,status=0,menubar=0,scrollbars=1'
//        ],
//    ],
//];

$GLOBALS['TCA'][$sModel]['columns']['btn']['config']['foreign_table_where'] = 'AND tx_hivecptcntbsbtn_domain_model_btn.hidden=0 AND tx_hivecptcntbsbtn_domain_model_btn.deleted=0 AND tx_hivecptcntbsbtn_domain_model_btn.sys_language_uid IN (-1,0)';


$GLOBALS['TCA'][$sModel]['columns']['cardwidth']['config']['items'] = array(
    array('---', 0),
    array('w-100', 1),
    array('w-75', 2),
    array('w-50', 3),
    array('w-25', 4)
);

$GLOBALS['TCA'][$sModel]['columns']['cardsizesm']['config']['items'] = array(
    array('---', 0),
    array('col-sm-3', 1),
    array('col-sm-4', 2),
    array('col-sm-6', 3),
    array('col-sm-12', 4)
);

$GLOBALS['TCA'][$sModel]['columns']['cardsizemd']['config']['items'] = array(
    array('---', 0),
    array('col-md-3', 1),
    array('col-md-4', 2),
    array('col-md-6', 3),
    array('col-md-12', 4)
);

$GLOBALS['TCA'][$sModel]['columns']['cardsizelg']['config']['items'] = array(
    array('---', 0),
    array('col-lg-3', 1),
    array('col-lg-4', 2),
    array('col-lg-6', 3),
    array('col-lg-12', 4)
);

$GLOBALS['TCA'][$sModel]['columns']['cardsizexl']['config']['items'] = array(
    array('---', 0),
    array('col-xl-1', 1),
    array('col-xl-2', 2),
    array('col-xl-3', 3),
    array('col-xl-4', 4),
    array('col-xl-6', 5),
    array('col-xl-12', 6)
);

$GLOBALS['TCA'][$sModel]['columns']['textalignment']['config']['items'] = array(
    array('---', 0),
    array('left', 1),
    array('center', 2),
    array('right', 3)
);

$GLOBALS['TCA'][$sModel]['columns']['imageposition']['config']['items'] = array(
    array('---', 0),
    array('top', 1),
    array('bottom', 2)
);

$GLOBALS['TCA'][$sModel]['columns']['backgroundcolor']['config']['items'] = array(
    array('---', 0),
    array('primary', 1),
    array('secondary', 2),
    array('tertiary', 3),
    array('success', 4),
    array('info', 5),
    array('warning', 6),
    array('danger', 7),
);

$GLOBALS['TCA'][$sModel]['columns']['bordercolor']['config']['items'] = array(
    array('---', 0),
    array('primary', 1),
    array('secondary', 2),
    array('tertiary', 3),
    array('success', 4),
    array('info', 5),
    array('warning', 6),
    array('danger', 7),
);



//$GLOBALS['TCA'][$sModel]['columns']['img']['config']['img'] = [
//    'appearance' => [
//        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
//    ],
//    'foreign_types' => [
//        '0' => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ],
//        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ],
//        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ],
//        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ],
//        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ],
//        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
//            'showitem' => '
//                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
//                            --palette--;;filePalette'
//        ]
//    ],
//    'maxitems' => 1
//];

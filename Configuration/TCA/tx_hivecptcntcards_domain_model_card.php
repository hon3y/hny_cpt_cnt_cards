<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card',
        'label' => 'backendtitle',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backendtitle,title,subtitle,text,image,headertext,footertext,cardwidth,cardsizesm,cardsizemd,cardsizelg,cardsizexl,textalignment,imageposition,enableimageoverlay,enablecardinverse,backgroundcolor,bordercolor,btn',
        'iconfile' => 'EXT:hive_cpt_cnt_cards/Resources/Public/Icons/tx_hivecptcntcards_domain_model_card.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backendtitle, title, subtitle, text, image, headertext, footertext, cardwidth, cardsizesm, cardsizemd, cardsizelg, cardsizexl, textalignment, imageposition, enableimageoverlay, enablecardinverse, backgroundcolor, bordercolor, btn',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backendtitle, title, subtitle, text, image, headertext, footertext, cardwidth, cardsizesm, cardsizemd, cardsizelg, cardsizexl, textalignment, imageposition, enableimageoverlay, enablecardinverse, backgroundcolor, bordercolor, btn, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hivecptcntcards_domain_model_card',
                'foreign_table_where' => 'AND tx_hivecptcntcards_domain_model_card.pid=###CURRENT_PID### AND tx_hivecptcntcards_domain_model_card.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'backendtitle' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.backendtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'subtitle' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.subtitle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'text' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.text',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'image' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.image',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'image',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:images.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']
            ),
        ],
        'headertext' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.headertext',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'footertext' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.footertext',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'cardwidth' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.cardwidth',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'cardsizesm' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.cardsizesm',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'cardsizemd' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.cardsizemd',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'cardsizelg' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.cardsizelg',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'cardsizexl' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.cardsizexl',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'textalignment' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.textalignment',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'imageposition' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.imageposition',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'enableimageoverlay' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.enableimageoverlay',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'enablecardinverse' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.enablecardinverse',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'backgroundcolor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.backgroundcolor',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'bordercolor' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.bordercolor',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'btn' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_cpt_cnt_cards/Resources/Private/Language/locallang_db.xlf:tx_hivecptcntcards_domain_model_card.btn',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_hivecptcntbsbtn_domain_model_btn',
                'MM' => 'tx_hivecptcntcards_card_btn_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'module' => [
                            'name' => 'wizard_edit',
                        ],
                        'type' => 'popup',
                        'title' => 'Edit', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.edit
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'module' => [
                            'name' => 'wizard_add',
                        ],
                        'type' => 'script',
                        'title' => 'Create new', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.add
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_hivecptcntbsbtn_domain_model_btn',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                    ],
                ],
            ],
            
        ],
    
    ],
];

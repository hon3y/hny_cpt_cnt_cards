<?php
namespace HIVE\HiveCptCntCards\Controller;

/***
 *
 * This file is part of the "hive_cpt_cnt_cards" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * CardController
 */
class CardController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * cardRepository
     *
     * @var \HIVE\HiveCptCntCards\Domain\Repository\CardRepository
     * @inject
     */
    protected $cardRepository = null;

    /**
     * action renderCard
     *
     * @return void
     */
    public function renderCardAction()
    {
        $aSettings = $this->settings;
        if (array_key_exists('bDebug', $aSettings) && array_key_exists('sDebugIp', $aSettings)) {
            $aDebugIp = explode(',', $aSettings['sDebugIp']);
            if ($aSettings['bDebug'] == '1' && (in_array($_SERVER['REMOTE_ADDR'], $aDebugIp) or $aSettings['sDebugIp'] == '*')) {
                \TYPO3\CMS\Core\Utility\DebugUtility::debug($aSettings, 'Debug: ' . __FILE__ . ' in Line: ' . __LINE__);
            }
        }
        $oButtons = $this->cardRepository->findByUidListOrderByList($aSettings['oHiveCptCntCards']['main']['mn']);
        $this->view->assign('oCards', $oButtons);
        $this->view->assign('aSettings', $aSettings);
    }
}

<?php
namespace HIVE\HiveCptCntCards\Domain\Repository;

/***
 *
 * This file is part of the "hive_cpt_cnt_cards" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Cards
 */
class CardRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    /**
     * findByUidListOrderByList
     *
     * @param string String containing uids
     * @return \HIVE\HiveCptCntCards\Domain\Model\Card
     */
    public function findByUidListOrderByList($uidList)
    {
        $uidArray = explode(',', $uidList);
        $result = [];
        foreach ($uidArray as $uid) {
            $result[] = $this->findByUid($uid);
        }
        return $result;
    }

    /**
     * Find by Uid
     *
     * @param int $uid
     * @return Card
     */
    public function findByUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectSysLanguage(FALSE);
        $query->getQuerySettings()->setRespectStoragePage(FALSE);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }
}

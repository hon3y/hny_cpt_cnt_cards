<?php
namespace HIVE\HiveCptCntCards\Domain\Model;

/***
 *
 * This file is part of the "hive_cpt_cnt_cards" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Card
 */
class Card extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendtitle
     *
     * @var string
     */
    protected $backendtitle = '';

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * subtitle
     *
     * @var string
     */
    protected $subtitle = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * headertext
     *
     * @var string
     */
    protected $headertext = '';

    /**
     * footertext
     *
     * @var string
     */
    protected $footertext = '';

    /**
     * cardwidth
     *
     * @var int
     */
    protected $cardwidth = 0;

    /**
     * cardsizesm
     *
     * @var int
     */
    protected $cardsizesm = 0;

    /**
     * cardsizemd
     *
     * @var int
     */
    protected $cardsizemd = 0;

    /**
     * cardsizelg
     *
     * @var int
     */
    protected $cardsizelg = 0;

    /**
     * cardsizexl
     *
     * @var int
     */
    protected $cardsizexl = 0;

    /**
     * textalignment
     *
     * @var int
     */
    protected $textalignment = 0;

    /**
     * imageposition
     *
     * @var int
     */
    protected $imageposition = 0;

    /**
     * enableimageoverlay
     *
     * @var bool
     */
    protected $enableimageoverlay = false;

    /**
     * enablecardinverse
     *
     * @var bool
     */
    protected $enablecardinverse = false;

    /**
     * backgroundcolor
     *
     * @var int
     */
    protected $backgroundcolor = 0;

    /**
     * bordercolor
     *
     * @var int
     */
    protected $bordercolor = 0;

    /**
     * btn
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn>
     */
    protected $btn = null;

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $image = null;

    /**
     * Returns the backendtitle
     *
     * @return string $backendtitle
     */
    public function getBackendtitle()
    {
        return $this->backendtitle;
    }

    /**
     * Sets the backendtitle
     *
     * @param string $backendtitle
     * @return void
     */
    public function setBackendtitle($backendtitle)
    {
        $this->backendtitle = $backendtitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the subtitle
     *
     * @return string $subtitle
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * Sets the subtitle
     *
     * @param string $subtitle
     * @return void
     */
    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the headertext
     *
     * @return string $headertext
     */
    public function getHeadertext()
    {
        return $this->headertext;
    }

    /**
     * Sets the headertext
     *
     * @param string $headertext
     * @return void
     */
    public function setHeadertext($headertext)
    {
        $this->headertext = $headertext;
    }

    /**
     * Returns the footertext
     *
     * @return string $footertext
     */
    public function getFootertext()
    {
        return $this->footertext;
    }

    /**
     * Sets the footertext
     *
     * @param string $footertext
     * @return void
     */
    public function setFootertext($footertext)
    {
        $this->footertext = $footertext;
    }

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->btn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Adds a Btn
     *
     * @param \HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btn
     * @return void
     */
    public function addBtn(\HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btn)
    {
        $this->btn->attach($btn);
    }

    /**
     * Removes a Btn
     *
     * @param \HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btnToRemove The Btn to be removed
     * @return void
     */
    public function removeBtn(\HIVE\HiveCptCntBsBtn\Domain\Model\Btn $btnToRemove)
    {
        $this->btn->detach($btnToRemove);
    }

    /**
     * Returns the btn
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn> $btn
     */
    public function getBtn()
    {
        return $this->btn;
    }

    /**
     * Sets the btn
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveCptCntBsBtn\Domain\Model\Btn> $btn
     * @return void
     */
    public function setBtn(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $btn)
    {
        $this->btn = $btn;
    }

    /**
     * Returns the cardwidth
     *
     * @return int $cardwidth
     */
    public function getCardwidth()
    {
        return $this->cardwidth;
    }

    /**
     * Sets the cardwidth
     *
     * @param int $cardwidth
     * @return void
     */
    public function setCardwidth($cardwidth)
    {
        $this->cardwidth = $cardwidth;
    }

    /**
     * Returns the cardsizesm
     *
     * @return int $cardsizesm
     */
    public function getCardsizesm()
    {
        return $this->cardsizesm;
    }

    /**
     * Sets the cardsizesm
     *
     * @param int $cardsizesm
     * @return void
     */
    public function setCardsizesm($cardsizesm)
    {
        $this->cardsizesm = $cardsizesm;
    }

    /**
     * Returns the cardsizemd
     *
     * @return int $cardsizemd
     */
    public function getCardsizemd()
    {
        return $this->cardsizemd;
    }

    /**
     * Sets the cardsizemd
     *
     * @param int $cardsizemd
     * @return void
     */
    public function setCardsizemd($cardsizemd)
    {
        $this->cardsizemd = $cardsizemd;
    }

    /**
     * Returns the cardsizelg
     *
     * @return int $cardsizelg
     */
    public function getCardsizelg()
    {
        return $this->cardsizelg;
    }

    /**
     * Sets the cardsizelg
     *
     * @param int $cardsizelg
     * @return void
     */
    public function setCardsizelg($cardsizelg)
    {
        $this->cardsizelg = $cardsizelg;
    }

    /**
     * Returns the textalignment
     *
     * @return int $textalignment
     */
    public function getTextalignment()
    {
        return $this->textalignment;
    }

    /**
     * Sets the textalignment
     *
     * @param int $textalignment
     * @return void
     */
    public function setTextalignment($textalignment)
    {
        $this->textalignment = $textalignment;
    }

    /**
     * Returns the imageposition
     *
     * @return int $imageposition
     */
    public function getImageposition()
    {
        return $this->imageposition;
    }

    /**
     * Sets the imageposition
     *
     * @param int $imageposition
     * @return void
     */
    public function setImageposition($imageposition)
    {
        $this->imageposition = $imageposition;
    }

    /**
     * Returns the enableimageoverlay
     *
     * @return bool $enableimageoverlay
     */
    public function getEnableimageoverlay()
    {
        return $this->enableimageoverlay;
    }

    /**
     * Sets the enableimageoverlay
     *
     * @param bool $enableimageoverlay
     * @return void
     */
    public function setEnableimageoverlay($enableimageoverlay)
    {
        $this->enableimageoverlay = $enableimageoverlay;
    }

    /**
     * Returns the boolean state of enableimageoverlay
     *
     * @return bool
     */
    public function isEnableimageoverlay()
    {
        return $this->enableimageoverlay;
    }

    /**
     * Returns the enablecardinverse
     *
     * @return bool $enablecardinverse
     */
    public function getEnablecardinverse()
    {
        return $this->enablecardinverse;
    }

    /**
     * Sets the enablecardinverse
     *
     * @param bool $enablecardinverse
     * @return void
     */
    public function setEnablecardinverse($enablecardinverse)
    {
        $this->enablecardinverse = $enablecardinverse;
    }

    /**
     * Returns the boolean state of enablecardinverse
     *
     * @return bool
     */
    public function isEnablecardinverse()
    {
        return $this->enablecardinverse;
    }

    /**
     * Returns the backgroundcolor
     *
     * @return int $backgroundcolor
     */
    public function getBackgroundcolor()
    {
        return $this->backgroundcolor;
    }

    /**
     * Sets the backgroundcolor
     *
     * @param int $backgroundcolor
     * @return void
     */
    public function setBackgroundcolor($backgroundcolor)
    {
        $this->backgroundcolor = $backgroundcolor;
    }

    /**
     * Returns the bordercolor
     *
     * @return int $bordercolor
     */
    public function getBordercolor()
    {
        return $this->bordercolor;
    }

    /**
     * Sets the bordercolor
     *
     * @param int $bordercolor
     * @return void
     */
    public function setBordercolor($bordercolor)
    {
        $this->bordercolor = $bordercolor;
    }

    /**
     * Returns the cardsizexl
     *
     * @return int $cardsizexl
     */
    public function getCardsizexl()
    {
        return $this->cardsizexl;
    }

    /**
     * Sets the cardsizexl
     *
     * @param int $cardsizexl
     * @return void
     */
    public function setCardsizexl($cardsizexl)
    {
        $this->cardsizexl = $cardsizexl;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
}

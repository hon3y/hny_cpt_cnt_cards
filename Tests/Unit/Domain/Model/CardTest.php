<?php
namespace HIVE\HiveCptCntCards\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class CardTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveCptCntCards\Domain\Model\Card
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveCptCntCards\Domain\Model\Card();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendtitle()
        );
    }

    /**
     * @test
     */
    public function setBackendtitleForStringSetsBackendtitle()
    {
        $this->subject->setBackendtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSubtitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getSubtitle()
        );
    }

    /**
     * @test
     */
    public function setSubtitleForStringSetsSubtitle()
    {
        $this->subject->setSubtitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'subtitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getText()
        );
    }

    /**
     * @test
     */
    public function setTextForStringSetsText()
    {
        $this->subject->setText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'text',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getImageReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getImage()
        );
    }

    /**
     * @test
     */
    public function setImageForFileReferenceSetsImage()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setImage($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'image',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHeadertextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHeadertext()
        );
    }

    /**
     * @test
     */
    public function setHeadertextForStringSetsHeadertext()
    {
        $this->subject->setHeadertext('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'headertext',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFootertextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFootertext()
        );
    }

    /**
     * @test
     */
    public function setFootertextForStringSetsFootertext()
    {
        $this->subject->setFootertext('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'footertext',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getCardwidthReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setCardwidthForIntSetsCardwidth()
    {
    }

    /**
     * @test
     */
    public function getCardsizesmReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setCardsizesmForIntSetsCardsizesm()
    {
    }

    /**
     * @test
     */
    public function getCardsizemdReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setCardsizemdForIntSetsCardsizemd()
    {
    }

    /**
     * @test
     */
    public function getCardsizelgReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setCardsizelgForIntSetsCardsizelg()
    {
    }

    /**
     * @test
     */
    public function getCardsizexlReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setCardsizexlForIntSetsCardsizexl()
    {
    }

    /**
     * @test
     */
    public function getTextalignmentReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setTextalignmentForIntSetsTextalignment()
    {
    }

    /**
     * @test
     */
    public function getImagepositionReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setImagepositionForIntSetsImageposition()
    {
    }

    /**
     * @test
     */
    public function getEnableimageoverlayReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getEnableimageoverlay()
        );
    }

    /**
     * @test
     */
    public function setEnableimageoverlayForBoolSetsEnableimageoverlay()
    {
        $this->subject->setEnableimageoverlay(true);

        self::assertAttributeEquals(
            true,
            'enableimageoverlay',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getEnablecardinverseReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getEnablecardinverse()
        );
    }

    /**
     * @test
     */
    public function setEnablecardinverseForBoolSetsEnablecardinverse()
    {
        $this->subject->setEnablecardinverse(true);

        self::assertAttributeEquals(
            true,
            'enablecardinverse',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getBackgroundcolorReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setBackgroundcolorForIntSetsBackgroundcolor()
    {
    }

    /**
     * @test
     */
    public function getBordercolorReturnsInitialValueForInt()
    {
    }

    /**
     * @test
     */
    public function setBordercolorForIntSetsBordercolor()
    {
    }

    /**
     * @test
     */
    public function getBtnReturnsInitialValueForBtn()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getBtn()
        );
    }

    /**
     * @test
     */
    public function setBtnForObjectStorageContainingBtnSetsBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $objectStorageHoldingExactlyOneBtn = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneBtn->attach($btn);
        $this->subject->setBtn($objectStorageHoldingExactlyOneBtn);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneBtn,
            'btn',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addBtnToObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->addBtn($btn);
    }

    /**
     * @test
     */
    public function removeBtnFromObjectStorageHoldingBtn()
    {
        $btn = new \HIVE\HiveCptCntBsBtn\Domain\Model\Btn();
        $btnObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $btnObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($btn));
        $this->inject($this->subject, 'btn', $btnObjectStorageMock);

        $this->subject->removeBtn($btn);
    }
}
